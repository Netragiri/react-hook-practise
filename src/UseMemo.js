import React, { useState,useMemo } from 'react'

function UseMemo() {
  const[count,setCount]=useState(0)
  const[item,setItem]=useState(5)


  //this function called when the count value is change with use of useMemo
 const  multicount=useMemo(()=>{
   console.log('multicount called')
    return count*5
  },[count])


  return (
    <div>
      <p>Count :{count}</p>
      <p>Item :{item}</p><br />
      <h2>This called when only count changes :{multicount}</h2>
      <button onClick={()=>setCount(count+1)}>Increment</button>
      <button onClick={()=>setItem(item*10)}>Increment item</button>
    </div>
  )
}

export default UseMemo
//main advantage of useMemo is to stop the function calling again and again if we need to call the it called