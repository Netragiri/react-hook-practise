import React,{useContext} from 'react'
import { Context } from './App'

function ContextApi() {
    const user=useContext(Context)
      return (
        <div>
          <p>{user}</p>
        </div>
      )
    }

export default ContextApi
