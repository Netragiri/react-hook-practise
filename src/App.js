import logo from './logo.svg';
import './App.css';
import UseMemo from './UseMemo';
import DrakMode from './DrakMode';
import { createContext,useContext } from 'react';
import ContextApi from './ContextApi';


const Context=createContext()
function App() {

  //for globally use context
 const name='netra'
  return (
    <div className="App">
      {/* <UseMemo /> */}
      {/* <DrakMode /> */}
      <Context.Provider value={name}>
        <ContextApi />
      </Context.Provider>
    </div>
  );
}

export {Context}
export default App;

